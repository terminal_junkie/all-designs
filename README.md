<img src="/Artwork/ftslogo.jpg" alt="image" width="100" height="auto">

----
### FTS EDC
A free and open-source EDC project by Fit To Survive LLC. Attribution required, remixing encouraged, **not for commercial use**. 

Follow @fts_edc for updates and photos

----
#### Printables

##### PLVG
- [V1](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/PLVGFINAL.stl)
##### M5StickC Plus Paracord Case
- [V1 (with loop)](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/fts_m5stickc_case.stl)
- [V1 (no loop)](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/fts_m5sttickc_case_noloop.stl)
##### Santa Muerte
A printable Santa Muerte figurine designed for carrying in your pocket! 
Designed by [@magicka3d](https://instagram.com/magicka3d). Coming in 2 versions, a fully rounded model, and a model with a flat base for standing upright. 
- [Rounded](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/muerteproto.stl?ref_type=heads)
- [Flat](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/muertefinal.stl?ref_type=heads)
##### Tech Spike Trainer
- [V1](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/techspikev5.stl?ref_type=heads)
#### Blocklite Protective Carry Case
- [Prototype](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/blocklitePROTO.stl)
- [Final Version](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/ftsblocklite.stl?ref_type=heads)
----
### Knucks
- [Daemon](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/knucks/daemon.stl)
- [knuckintosh](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/knucks/knuckintosh.stl)
- [Sudo](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/knucks/sudo.stl)
- [TJknuck](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/knucks/tjknuck2.stl)
----
### Scales
#### Elvia C 
- [LV](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/elviac_LV.stl)
- [Hive](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/elviac_hive.stl)
- [Reaper](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/elviac_reaper.stl)
- [Corrugated](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/elviac_corrugated.stl)
#### La Bruja
- [Blank](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/brujablank.stl)
- [Corrugated](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/bruja_corrugated.stl)
- [Reaper](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/scales/bruja_reaper.stl)
----
### RFID/NFC 
###### (Haptic Slider and RipBox require 3mm x 3mm magnets)
- [RipBox](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/RipBox.stl)
- [FTS Haptic Access Slider](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/haptic_access_slider.stl)
- [RFID/NFC Scap Insert](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season3/rfid_nfc_scap_insert.stl)
----
### Paracord Beads
#### Basic
- [Duplici](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season1/duplicifinal.stl)
- [EXO-GRIP](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season1/exo-gripfinal.stl)
- [Florero](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season1/florerofinal.stl)
- [Lantern](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season1/lantern.stl)
- [TRI-GRIP](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season1/tri-grip.stl)

#### Bootleg Series
- [VL](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/vlbead.stl)
- [CC](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/CCbead.stl)


#### Pillz Series 
- [PLAYSTATIONZ](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/playstationz.stl)
- [PLAYBOYZ](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/playboyz.stl)
- [BISHI](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/bsishi.stl)
- [ROLLZ](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/rolls.stl)

#### Other
- [Glider Goon](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season1/glidergoonbeadfinal.stl)
- [D00M MINI](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/d00mmini.stl)
- [D00M](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/d00m.stl)
- [GFACE MINI](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/ghostfacemini.stl)
- [GFACE](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/ghostfacelarge.stl)
- [NOFACE](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/noface.stl)
- ["A"](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season2/beads/A.stl)
- [Slasher](https://gitlab.com/ftsedc1/all-designs/-/blob/main/Season4/beads/slashermask.stl)










